﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using KeepDistance.Communicate.enums;
using KeepDistance.Database.models;
using KeepDistance.Server.models;
using RestSharp;
using Xunit;

namespace Tests
{
    public class RestTests
    {
        private RestClient _client;

        public RestTests()
        {
            _client = new RestClient("https://localhost:5001/api/telemetry");
        }

        [Fact]
        public void ShouldCreateSession()
        {
            var networkSession = new NetworkTelemetry
            {
                Info = new UserInfo
                {
                    IsSickCoronavirus = true,
                    Name = "test",
                    Token = "test_token_2"
                },
                Session = new TelemetrySession
                {
                    Caller = "user",
                    Recepient = "rec",
                    StartTime = DateTime.Now,
                    States = new List<TelemetryState>
                    {
                        new TelemetryState
                        {
                            Altitude = 0,
                            Latitude = 2,
                            Longitude = 3,
                            Time = DateTime.Now
                        }
                    }
                }
            };

            var request = new RestRequest("", DataFormat.Json).AddJsonBody(networkSession);

            var response = _client.Post<object>(request);

            Assert.True(response.IsSuccessful);
            Assert.True(response.StatusCode == HttpStatusCode.OK);
        }

        [Fact]
        public void ShouldUpdateSession()
        {
            var networkSession = new NetworkTelemetry
            {
                Info = new UserInfo
                {
                    IsSickCoronavirus = true,
                    Name = "test",
                    Token = "test_token_2"
                },
                Session = new TelemetrySession
                {
                    Caller = "user",
                    Recepient = "rec",
                    StartTime = DateTime.Now,
                    States = new List<TelemetryState>
                    {
                        new TelemetryState
                        {
                            Altitude = 1000,
                            Latitude = 2000,
                            Longitude = 3000,
                            Time = DateTime.Now
                        }
                    }
                }
            };

            var request = new RestRequest("", DataFormat.Json).AddJsonBody(networkSession);

            var response = _client.Patch<object>(request);

            Assert.True(response.IsSuccessful);
            Assert.True(response.StatusCode == HttpStatusCode.OK);
        }
    }
}