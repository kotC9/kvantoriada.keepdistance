﻿using System.IO;

namespace KeepDistance.Database
{
    public class DatabaseProvider
    {
        private readonly string _folder;
        private readonly string _databaseName;

        public string Path => $"{System.IO.Path.Combine(_folder, _databaseName)}.db";

        public DatabaseProvider(string folder, string databaseName)
        {
            _folder = folder;
            _databaseName = databaseName;

            CheckFolder();
        }

        private void CheckFolder()
        {
            if (!Directory.Exists(_folder))
            {
                Directory.CreateDirectory(_folder);
            }
        }
    }

}