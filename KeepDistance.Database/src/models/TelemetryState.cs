﻿﻿using System;
 using System.Collections.Generic;
 using System.Linq;
 using KeepDistance.Communicate.enums;
 using Xamarin.Essentials;

namespace KeepDistance.Database.models
{
    public class TelemetryState
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double? Altitude { get; set; }
        public Dictionary<string, int> Dict { get; set; }

        public SafeDistance Distance =>
            Dict!=null && Dict.Count>0 ? SafeDistance.NotKeeped : SafeDistance.Keeped;
        public DateTime Time { get; set; }

        public TelemetryState(Location location, Dictionary<string, int> dict)
        {
            Altitude = location.Altitude;
            Latitude = location.Latitude;
            Longitude = location.Longitude;
            Dict = dict;
            Time = DateTime.Now;
        }

        public TelemetryState()
        {
            
        }
    }
}