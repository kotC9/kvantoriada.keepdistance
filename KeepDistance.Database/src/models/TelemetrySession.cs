﻿﻿using System;
using System.Collections.Generic;

namespace KeepDistance.Database.models
{
    public class TelemetrySession
    {
        public int Id { get; set; }
        
        //user
        public string Caller { get; set; } = "Deidara";
        public string Recepient { get; set; }
        public List<TelemetryState> States { get; set; }
        public DateTime StartTime { get; set; }
    }
}