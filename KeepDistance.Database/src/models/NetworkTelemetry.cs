﻿using KeepDistance.Database.models;

namespace KeepDistance.Server.models
{
    public class NetworkTelemetry
    {
        public int Id { get; set; }
        public UserInfo Info { get; set; }
        public TelemetrySession Session { get; set; }
    }
}