﻿namespace KeepDistance.Database.models
{
    public class UserInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsSickCoronavirus { get; set; }
        public string Token { get; set; }
    }
}