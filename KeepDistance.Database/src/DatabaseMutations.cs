﻿using System;
using System.Collections.Generic;
using System.Linq;
using KeepDistance.Communicate.enums;
using KeepDistance.Database.models;
using LiteDB;
using Xamarin.Essentials;

namespace KeepDistance.Database
{
    public class DatabaseMutations
    {
        private string _path;

        public DatabaseMutations(DatabaseProvider provider)
        {
            _path = provider.Path;
        }

        public void CreateEmptySession()
        {
            try
            {
                using var db = new LiteDatabase(_path);
                var sessions = db.GetCollection<TelemetrySession>("sessions");

                var session = new TelemetrySession
                {
                    StartTime = DateTime.Now,
                    States = new List<TelemetryState>()
                };
                sessions.Insert(session);
            }
            catch
            {
            }
        }

        public void AddState(TelemetryState state)
        {
            state.Time = DateTime.Now;
            try
            {
                using var db = new LiteDatabase(_path);
                var sessions = db.GetCollection<TelemetrySession>("sessions");

                var oldData = sessions.FindById(sessions.Max());
                oldData.States.Add(state);
                sessions.Update(oldData);
            }
            catch
            {
            }
        }

        public TelemetryState GetStateById(int id)
        {
            try
            {
                using var db = new LiteDatabase(_path);
                var sessions = db.GetCollection<TelemetrySession>("sessions");
                
                var session = sessions.FindById(sessions.Max());
                var state = session.States[id];
                return state;
            }
            catch
            {
                return null;
            }
        }

        public TelemetrySession GetCurrentSession()
        {
            try
            {
                using var db = new LiteDatabase(_path);
                var sessions = db.GetCollection<TelemetrySession>("sessions");
                var session = sessions.FindById(sessions.Max());
                return session;
            }
            catch
            {
                return null;
            }
        }

        public bool CreateUser(UserInfo userInfo)
        {
            try
            {
                using var db = new LiteDatabase(_path);
                var info = db.GetCollection<UserInfo>("info");
                info.DeleteAll();
                info.Insert(userInfo);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public UserInfo GetUserInfo()
        {
            try
            {
                using var db = new LiteDatabase(_path);
                var info = db.GetCollection<UserInfo>("info").Max(id => id);
                
                return info;
            }
            catch
            {
                return null;
            }
        }
    }
}