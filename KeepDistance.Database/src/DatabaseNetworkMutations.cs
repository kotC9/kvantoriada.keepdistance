﻿using System;
using System.Collections.Generic;
using KeepDistance.Database.models;
using KeepDistance.Server.models;
using LiteDB;

namespace KeepDistance.Database
{
    public class DatabaseNetworkMutations
    {
        private string _path;

        public DatabaseNetworkMutations(DatabaseProvider provider)
        {
            _path = provider.Path;
        }
        public bool Create(NetworkTelemetry telemetry)
        {
            try
            {
                using var db = new LiteDatabase(_path);
                var sessions = db.GetCollection<NetworkTelemetry>(telemetry.Info.Token);
                sessions.Insert(telemetry);

                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public bool Update(NetworkTelemetry telemetry)
        {
            try
            {
                using var db = new LiteDatabase(_path);
                var sessions = db.GetCollection<NetworkTelemetry>(telemetry.Info.Token);
                
                var oldData = sessions.FindById(sessions.Max());
                if (oldData != null)
                {
                    oldData.Session.States.AddRange(telemetry.Session.States);
                    sessions.Update(oldData);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}