﻿namespace KeepDistance.Communicate.enums
{
    public enum SafeDistance : byte
    {
        NotKeeped = 0,
        Keeped = 1
    }
}