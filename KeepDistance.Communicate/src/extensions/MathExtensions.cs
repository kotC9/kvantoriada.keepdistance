﻿

using System;

namespace KeepDistance.Communicate.extensions
{
    public static class MathExtensions
    {
        public static int ToCentimetres(this int dbm, double freqInMHz) {
            var exp = (27.55 - (20 * Math.Log10(freqInMHz)) + Math.Abs(dbm)) / 20.0;
            return (int) (Math.Pow(10.0, exp)*100);
        }
    }
}