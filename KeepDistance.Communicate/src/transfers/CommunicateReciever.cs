﻿using System;
using System.Threading.Tasks;
using Android.Util;
using KeepDistance.Communicate.Models;
using KeepDistance.Communicate.Providers;

namespace KeepDistance.Communicate.Transfers
{
    public class CommunicateReader
    {
        public event EventHandler<ReadedEventArgs> Received;
        public event EventHandler<FailedEventArgs> Failed;
        private readonly ICommunicateProvider _provider;
        private bool _isStarted;

        public CommunicateReader(ICommunicateProvider communicateProvider)
        {
            Failed += (sender, args) => { Log.Info("receiver", $"{args.Message}"); };

            _provider = communicateProvider;
        }

        public void EndRead()
        {
            _isStarted = false;
        }

        public async void BeginReadAsync()
        {
            //await Task.Run(BeginRead);
            BeginRead();
        }

        private async void BeginRead()
        {
            _isStarted = true;
            while (_isStarted)
            {
                try
                {
                    var (success, rawPackage) = ReadRawPackage();
/*
                    var success = true;
                    var r = new Random();
                    string rawPackage;

                    var a = r.Next(0, 4);
                    if (a == 0)
                    {
                        rawPackage =
                            $"$3 SafeDistanceNode_1 {r.Next(-40, 40)} SafeDistanceNode_2 {r.Next(-40, 40)} SafeDistanceNode_3 {r.Next(-40, 40)};\r";
                    }
                    else if (a == 1)
                    {
                        rawPackage = $"$2 SafeDistanceNode_2 {r.Next(-40, 40)} SafeDistanceNode_3 {r.Next(-40, 40)};\r";
                    }
                    else
                    {
                        rawPackage = $"$0 ;";
                    }*/

                    if (success)
                    {
                        Received?.Invoke(this, new ReadedEventArgs(rawPackage));
                    }
                    else
                    {
                        Logger("can't read");
                    }
                }
                catch
                {
                    Logger("receiver failed");
                }

                await Task.Delay(10000);
            }
        }

        private (bool, string) ReadRawPackage()
        {
            var rawPackage = "";

            var element = "p";
            while (element != "\n")
            {
                if (!_provider.Read(out element))
                {
                    return (false, null);
                }

                if (element != "\n")
                {
                    rawPackage += element;
                }
            }

            return (true, rawPackage);
        }

        private void Logger(string log)
        {
            Log.Info("receiver", $"{log}");
        }
    }
}