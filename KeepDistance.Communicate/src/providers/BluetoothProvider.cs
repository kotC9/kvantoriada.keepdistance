﻿using System.Collections.Generic;
using System.Linq;
using Android.Bluetooth;
using Android.Util;
using Java.Util;
using KeepDistance.Communicate.Extensions;
using KeepDistance.Communicate.Models;
using Stream = System.IO.Stream;

namespace KeepDistance.Communicate.Providers
{
    public class BluetoothProvider : ICommunicateProvider
    {
        private readonly BluetoothAdapter _bluetoothAdapter;
        private readonly UUID _uuid;
        private BluetoothSocket _btSocket;
        private Stream _inputStream;
        private Stream _outputStream;
        public DeviceItem Device { get; set; }

        public List<DeviceItem> Devices => GetDevices();

        public BluetoothProvider()
        {
            _uuid = UUID.FromString("00001101-0000-1000-8000-00805F9B34FB");
            _bluetoothAdapter = BluetoothAdapter.DefaultAdapter;


            if (HasAdapter())
            {
                if (_bluetoothAdapter.IsEnabled)
                {
                    _bluetoothAdapter.StartDiscovery();
                }

                if (!Enabled())
                {
                    _bluetoothAdapter.Enable();
                }
            }
        }

        public bool Connect()
        {
            if (!HasAdapter() || !Enabled() || Device == null)
            {
                return false;
            }
            
            try
            {
                
                var device = _bluetoothAdapter.GetRemoteDevice(Device.Address);
                _bluetoothAdapter.CancelDiscovery();
                _btSocket = device.CreateRfcommSocketToServiceRecord(_uuid);

                _inputStream = _btSocket.InputStream;
                _outputStream = _btSocket.OutputStream;

                _btSocket.Connect();

                Log.Info("Bluetooth Provider", "Connected");

                return true;
            }
            catch
            {
                Log.Info("Bluetooth Provider", "Failed connection");
                Close();
                return false;
            }
        }

        public bool Write(string data)
        {
            if (!Connected() || !Enabled() || !_outputStream.CanWrite)
            {
                return false;
            }

            var javaData = new Java.Lang.String(data);
            var msgBuffer = javaData.GetBytes();
            
            _outputStream?.Write(msgBuffer, 0, msgBuffer.Length);
            return true;
        }

        public bool Read(out string data)
        {
            data = null;

            if (!Connected() || !Enabled() || !_inputStream.CanRead)
            {
                return false;
            }

            var buffer = new byte[1];
            if (_inputStream?.Read(buffer, 0, buffer.Length) <= 0)
            {
                return false;
            }
            
            //byte[] -> string
            data = buffer.String();
            return true;
        }

        public void Close()
        { 
            _btSocket?.Close();

            _bluetoothAdapter?.StartDiscovery();
        }

        private List<DeviceItem> GetDevices()
        {
            if (!HasAdapter())
            {
                return null;
            }

            if (!Enabled())
            {
                _bluetoothAdapter.Enable();
            }

            return _bluetoothAdapter.BondedDevices
                .Select(bluetoothDevice =>
                    new DeviceItem
                    {
                        Address = bluetoothDevice.Address,
                        Name = bluetoothDevice.Name
                    })
                .ToList();
        }

        private bool Connected()
        {
            return _btSocket.IsConnected;
        }

        private bool Enabled()
        {
            return _bluetoothAdapter.IsEnabled;
        }

        private bool HasAdapter()
        {
            return _bluetoothAdapter != null;
        }
    }
}