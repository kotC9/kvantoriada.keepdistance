﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Util;
using KeepDistance.Communicate.enums;
using KeepDistance.Communicate.extensions;
using KeepDistance.Communicate.Extensions;
using KeepDistance.Communicate.Models;
using KeepDistance.Communicate.Transfers;

namespace KeepDistance.Communicate
{
    public class CommunicateMiddleware
    {
        public event EventHandler<ReceivedEventArgs> Received;
        public event EventHandler<FailedEventArgs> Failed;

        private readonly CommunicateReader _reader;

        private bool _failed;

        public bool IsStarted { get; private set; }

        public CommunicateMiddleware(CommunicateReader reader)
        {
            _reader = reader;

            _reader.Failed += OnFailed;

            _reader.Received += OnReceive;

            Received += (sender, args) => { Log.Info("receiver", "succeful received"); };
        }

        private void OnReceive(object sender, ReadedEventArgs e)
        {
            try
            {
                var package = PackageFrom(e.RawPackage);
                if (package != null)
                {
                    Received?.Invoke(sender, new ReceivedEventArgs(package));
                }
            }
            catch
            {
                Failed?.Invoke(this, new FailedEventArgs("middleware failed"));
            }
        }

        private void OnFailed(object sender, FailedEventArgs e)
        {
            if (_failed) return;

            _failed = true;
            Failed?.Invoke(this, new FailedEventArgs("middleware failed"));
        }

        public void Begin()
        {
            _reader.BeginReadAsync();
            IsStarted = true;
        }

        public void End()
        {
            _reader.EndRead();
            IsStarted = false;
        }

        private ReceivedPackage PackageFrom(string raw)
        {
            if (raw[0] != '$' || raw.Length <= 2)
            {
                return null;
            }

            //удаление $ и ;\r
            raw = raw[1..^2];
            var rawArray = raw.Split();

            if (!rawArray[0].IsInt() || rawArray.Length % 2 == 0)
                return null;

            var count = int.Parse(rawArray[0]);

            var output = new Dictionary<string, int>();

            for (var i = 1; i < count*2; i += 2)
            {
                if (int.TryParse(rawArray[i + 1], out var distance))
                    output[rawArray[i]] = distance.ToCentimetres(2400);
                else
                    return null;
            }

            return new ReceivedPackage {Nodes = output};
        }
    }
}