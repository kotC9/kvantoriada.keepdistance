﻿namespace KeepDistance.Communicate.Models
{
    public class FailedEventArgs
    {
        public string Message { get; }

        public FailedEventArgs(string message)
        {
            Message = message;
        }
    }
}