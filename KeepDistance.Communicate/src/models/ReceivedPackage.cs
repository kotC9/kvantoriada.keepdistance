﻿

using System.Collections.Generic;
using Java.Util;
using KeepDistance.Communicate.enums;

namespace KeepDistance.Communicate.Models
{
    public class ReceivedPackage
    {
        public Dictionary<string, int> Nodes;
    }
    
}