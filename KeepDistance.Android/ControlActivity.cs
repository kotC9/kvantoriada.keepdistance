﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using AndroidX.Core.App;
using AndroidX.RecyclerView.Widget;
using KeepDistance.Android.extensions;
using KeepDistance.Android.location;
using KeepDistance.Android.models;
using KeepDistance.Communicate;
using KeepDistance.Communicate.enums;
using KeepDistance.Communicate.Models;
using KeepDistance.Communicate.Transfers;
using KeepDistance.Database;
using KeepDistance.Database.models;
using KeepDistance.Tools;
using OxyPlot;
using OxyPlot.Xamarin.Android;
using Xamarin.Essentials;
using ActionBar = Android.App.ActionBar;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using Environment = System.Environment;

namespace KeepDistance.Android
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppThemeNoActionBar")]
    public class ControlActivity : AppCompatActivity
    {
        private const int Delay = 15000;

        private CommunicateMiddleware _communicateMiddleware;
        private GeolocationMiddleware _geolocationMiddleware;
        private DatabaseMutations _databaseMutations;

        private TextView _distanceTextView;
        private MainStatePlotter _mainStatePlotter;
        private Dictionary<string, NodeStatePlotter> _nodeStatePlotters;

        private TelemetryState _state;

        private bool _canAlarm = false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_control);

            InitializeComponents();

            new ActionTimer(Delay, SaveState).Start();
            new ActionTimer(Delay, ResetAlarm).Start();
        }

        private void ResetAlarm()
        {
            _canAlarm = true;
        }

        private void InitializeComponents()
        {
            InitializeLayoutComponents();
            InitializeCommunication();
            InitializeDatabase();

            _state = new TelemetryState();
            _nodeStatePlotters = new Dictionary<string, NodeStatePlotter>();
        }

        private void SaveState()
        {
            if (_state.Altitude == 0 && _state.Longitude == 0 && _state.Latitude == 0 || _state.Dict == null)
                return;

            RunOnUiThread(() =>
            {
                _distanceTextView.Text = _state.Distance == SafeDistance.Keeped
                    ? "Дистанция соблюдается"
                    : "Дистанция не соблюдается";
            });

            RunOnUiThread(DrawPlots);
            _databaseMutations.AddState(_state);

            if (_state.Distance == SafeDistance.NotKeeped)
                TryAlarm();
        }

        private void TryAlarm()
        {
            if (!_canAlarm) return;
            
            CreateNotification("Дистанция нарушена!", $"Время нарушения - {_state.Time}");
            
            //vibration
            var v = (Vibrator) GetSystemService(VibratorService);
            v.Vibrate(VibrationEffect.CreateOneShot(500, VibrationEffect.DefaultAmplitude));
        }
        private void CreateNotification(string title, string content)
        {
            var builder = new NotificationCompat.Builder(this, "KeepDistanceNotification")
                .SetContentTitle(title)
                .SetContentText(content)
                .SetSmallIcon(Resource.Drawable.ic_connect);

// Build the notification:
            var notification = builder.Build();

// Get the notification manager:
            var notificationManager =
                GetSystemService (Context.NotificationService) as NotificationManager;

// Publish the notification:
            const int notificationId = 0;
            notificationManager?.Notify (notificationId, notification);
        }

        private void DrawPlots()
        {
            DrawMainPlot();
            DrawNodePlots();
        }

        private void DrawNodePlots()
        {
            var layout = FindViewById<LinearLayout>(Resource.Id.nodePlotsLayout);
            foreach (var (key, _) in _state.Dict)
            {
                if (!_nodeStatePlotters.ContainsKey(key))
                {
                    _nodeStatePlotters[key] =
                        new NodeStatePlotter(layout.AddPlot(this), $"Статистика с {key}", _mainStatePlotter.PrevX);
                    _nodeStatePlotters[key].Model.TrackerChanged += NodePlotPointClicked;
                }
            }

            foreach (var (key, plotter) in _nodeStatePlotters)
            {
                plotter.DrawDistance(_state.Dict.ContainsKey(key) ? _state.Dict[key] : 150);
            }
        }

        private void DrawMainPlot()
        {
            if (_state.Distance == SafeDistance.Keeped)
                _mainStatePlotter.DrawKeep();
            else
                _mainStatePlotter.DrawNotKeep();


            Log.Info("main plotter", "successful drawed");
        }

        private void InitializeDatabase()
        {
            _databaseMutations = Global.DbService;
            _databaseMutations.CreateEmptySession();
        }

        private void InitializeCommunication()
        {
            _communicateMiddleware = new CommunicateMiddleware(
                new CommunicateReader(Global.Provider));
            _geolocationMiddleware = new GeolocationMiddleware(10000);

            if (!_communicateMiddleware.IsStarted)
            {
                _communicateMiddleware.Begin();
            }

            _communicateMiddleware.Failed += OnFailed;
            _communicateMiddleware.Received += OnBluetoothDataReceived;


            _geolocationMiddleware.BeginAsync();
            _geolocationMiddleware.Received += OnLocationReceived;
        }

        private void OnLocationReceived(object sender, LocationEventArgs e)
        {
            var location = e.Geolocation;
            _state.Altitude = location.Altitude;
            _state.Latitude = location.Latitude;
            _state.Longitude = location.Longitude;
        }

        private void OnBluetoothDataReceived(object sender, ReceivedEventArgs e)
        {
            var data = e.Package;
            _state.Dict = data.Nodes;

            var text = _state.Distance == SafeDistance.Keeped
                ? "Дистанция соблюдается"
                : "Дистанция не соблюдается";

            RunOnUiThread(() => { _distanceTextView.Text = text; });
        }

        private void OnFailed(object sender, FailedEventArgs e)
        {
            _communicateMiddleware.End();
            _geolocationMiddleware.End();
            Global.Provider.Close();
            Finish();
        }

        private void InitializeLayoutComponents()
        {
            _distanceTextView = FindViewById<TextView>(Resource.Id.textViewDistance);
            _distanceTextView.Text = "Дистанция соблюдается";

            var plot = FindViewById<PlotView>(Resource.Id.plotViewDistances);
            _mainStatePlotter = new MainStatePlotter(plot, "Общая статистика соблюдения дистанции");
            _mainStatePlotter.Model.TrackerChanged += MainPlotPointClicked;
        }

        private void NodePlotPointClicked(object sender, TrackerEventArgs e)
        {
            var xPoint = e.HitResult.DataPoint.X;
            var state = _databaseMutations.GetStateById((int) (xPoint * 4));

            var model = (PlotModel) sender;
            
            //ужасный способ, знаю.
            var key = model.Title.Split()[2];
            
            var time = state.Time;
            var text = $"{time.Hour}:{time.Minute}:{time.Second} {time.Day} {time:MMMM} {time.Year}\r\n\r\n";
            if (state.Dict.ContainsKey(key))
            {
                text += $"Дистанция до объекта равна {state.Dict[key]} см";
                CreatePlotDialog(state, text, SafeDistance.NotKeeped);
            }
            else
            {
                CreatePlotDialog(state, text, SafeDistance.Keeped);
            }
            
        }

        private void MainPlotPointClicked(object sender, TrackerEventArgs e)
        {
            var xPoint = e.HitResult.DataPoint.X;
            var state = _databaseMutations.GetStateById((int) (xPoint * 4));

            var time = state.Time;
            CreatePlotDialog(state, $"{time.Hour}:{time.Minute}:{time.Second} {time.Day} {time:MMMM} {time.Year}", state.Distance);
        }

        private void CreatePlotDialog(TelemetryState state, string content, SafeDistance distance)
        {
            var layoutInflater = LayoutInflater.From(this);
            var view = layoutInflater.Inflate(Resource.Layout.dialog_state, null);
            var alertbuilder = new AlertDialog.Builder(this);
            alertbuilder.SetView(view);
            alertbuilder.SetNegativeButton("Cancel", (sender, args) => { alertbuilder.Dispose(); });
            var dialogTitle = view.FindViewById<TextView>(Resource.Id.dialog_state_title);
            var dialogDate = view.FindViewById<TextView>(Resource.Id.dialog_state_date);

            dialogDate.Text = content;
            if (distance == SafeDistance.NotKeeped)
            {
                dialogTitle.Text = "Дистанция не соблюдается";
                alertbuilder.SetPositiveButton("Open map", OpenMapClick);
            }
            else
            {
                dialogTitle.Text = "Дистанция соблюдается";
            }

            var dialog = alertbuilder.Create();
            dialog.Show();

            void OpenMapClick(object sender, DialogClickEventArgs e)
            {
                var location = state.Altitude.HasValue
                    ? new Location(state.Latitude, state.Longitude, state.Altitude.Value)
                    : new Location(state.Latitude, state.Longitude);
                Map.OpenAsync(location);
            }
        }

        protected override void OnDestroy()
        {
            _communicateMiddleware.End();
            _geolocationMiddleware.End();
            base.OnDestroy();
        }
    }
}