﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Util;
using KeepDistance.Android.enums;
using KeepDistance.Android.models;
using KeepDistance.Communicate.Models;
using Xamarin.Essentials;

namespace KeepDistance.Android.location
{
    public class GeolocationProvider
    {
        private CancellationTokenSource _cts;
        public event EventHandler<LocationFailedEventArgc> Failed;
        
        public async Task<Location> GetCurrentLocation()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Best, TimeSpan.FromSeconds(10));
                _cts = new CancellationTokenSource();
                var location = await Geolocation.GetLocationAsync(request, _cts.Token);

                if (location != null)
                {
                    Log.Info("Geolocation provider", $"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");
                    return location;
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                Failed?.Invoke(this, new LocationFailedEventArgc(LocationException.FeatureNotSupported, fnsEx));
            }
            catch (FeatureNotEnabledException fneEx)
            {
                Failed?.Invoke(this, new LocationFailedEventArgc(LocationException.FeatureNotEnabled, fneEx));
            }
            catch (PermissionException pEx)
            {
                Failed?.Invoke(this, new LocationFailedEventArgc(LocationException.Permission, pEx));
            }
            catch (Exception ex)
            {
                Failed?.Invoke(this, new LocationFailedEventArgc(LocationException.Unable, ex));
            }

            return null;
        }

        public void Cancel()
        {
            if (_cts != null && !_cts.IsCancellationRequested)
                _cts.Cancel();
        }
    }
}