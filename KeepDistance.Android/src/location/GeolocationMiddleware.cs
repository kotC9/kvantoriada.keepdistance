﻿using System;
using System.Threading.Tasks;
using Android.Views;
using KeepDistance.Android.models;
using KeepDistance.Communicate.Models;

namespace KeepDistance.Android.location
{
    public class GeolocationMiddleware
    {
        private readonly int _delayInMs;
        private readonly GeolocationProvider _geolocationProvider;
        private bool _started = false;
        public event EventHandler<LocationEventArgs> Received;
        public event EventHandler<LocationFailedEventArgc> Failed;
        
        public GeolocationMiddleware(int delayInMs)
        {
            _delayInMs = delayInMs;
            _geolocationProvider = new GeolocationProvider();
            _geolocationProvider.Failed += (sender, args) => Failed?.Invoke(sender, args);
        }

        public async void BeginAsync()
        {
            await Task.Run(() =>
            {
                _started = true;
                while (_started)
                {
                    GetLocation();
                }
            });
        }
        private void GetLocation()
        {
            var location = _geolocationProvider.GetCurrentLocation().Result;
            if (location != null)
            {
                Received?.Invoke(this, new LocationEventArgs(location));
            }
        }

        public void End()
        {
            _started = false;
            _geolocationProvider.Cancel();
        }
    }
}