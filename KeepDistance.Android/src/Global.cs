﻿using KeepDistance.Communicate.Providers;
using KeepDistance.Database;

namespace KeepDistance.Android
{
    //TODO: change static class to android service
    public static class Global
    {
        public static ICommunicateProvider Provider;
        public static DatabaseMutations DbService;
    }
}