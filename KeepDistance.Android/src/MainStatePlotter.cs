﻿using Android.Graphics;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;

namespace KeepDistance.Android
{
    public class MainStatePlotter : BasePlotter
    {
        public new PlotModel Model => base.Model;
        public double PrevX => CurrentX-Step; 
        private readonly LineSeries _seriesNotKeep = new LineSeries
        {
            Color = OxyColors.Transparent, MarkerType = MarkerType.Circle, MarkerStrokeThickness = 1,
            MarkerStroke = OxyColor.FromRgb(255, 104, 89),
            MarkerFill = OxyColor.FromRgb(255, 182, 175)
        };

        private readonly LineSeries _seriesKeep = new LineSeries
        {
            Color = OxyColors.Transparent, MarkerType = MarkerType.Circle, MarkerStrokeThickness = 1,
            MarkerStroke = OxyColor.FromRgb(87, 115, 250),
            MarkerFill = OxyColor.FromRgb(169, 183, 252)
        };
        
        public MainStatePlotter(PlotView plot, string title, double minY = 0, double maxY = 60, int minX = 0, int maxX = 6) : 
            base(plot, title, minX, maxX, minY, maxY)
        {
            Model.Axes[0].Title = "Состояние";
            Model.Axes[0].TextColor = OxyColors.Transparent;
            Model.Series.Add(_seriesNotKeep);
            Model.Series.Add(_seriesKeep);
        }
        
        public void DrawKeep()
        {
            DrawRealtime(_seriesKeep, 40);
            Update();
        }

        public void DrawNotKeep()
        {
            DrawRealtime(_seriesNotKeep, 20);
            Update();
        }
    }
}