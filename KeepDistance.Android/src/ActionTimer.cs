﻿﻿using System;
using System.Threading.Tasks;

namespace KeepDistance.Tools
{
    public class ActionTimer
    {
        private readonly int _delayInMillisec;
        private readonly Action _action;

        private bool _started = false;
        public ActionTimer(int delayInMillisec, Action action)
        {
            _delayInMillisec = delayInMillisec;
            _action = action;
        }

        public async void Start()
        {
            _started = true;
            while (_started)
            {
                await Task.Delay(_delayInMillisec);

                _action?.Invoke();
            }
        }

        public void Stop()
        {
            _started = false;
        }
    }
}