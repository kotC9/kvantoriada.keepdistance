﻿using Xamarin.Essentials;

namespace KeepDistance.Android.models
{
    public class LocationEventArgs
    {
        public Location Geolocation { get; }

        public LocationEventArgs(Location location)
        {
            Geolocation = location;
        }
    }
}