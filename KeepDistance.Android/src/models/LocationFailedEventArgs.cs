﻿using KeepDistance.Android.enums;

namespace KeepDistance.Android.models
{
    public class LocationFailedEventArgc
    {
        public LocationException Exception { get; }
        public System.Exception Ex { get; }

        public LocationFailedEventArgc(LocationException exception, System.Exception ex)
        {
            Ex = ex;
            Exception = exception;
        }
    }
}