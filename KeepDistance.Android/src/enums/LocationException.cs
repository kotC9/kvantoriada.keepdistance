﻿namespace KeepDistance.Android.enums
{
    public enum LocationException
    {
        FeatureNotSupported,
        FeatureNotEnabled,
        Permission,
        Unable
    }
}