﻿using System.Threading.Tasks;
using KeepDistance.Database;
using KeepDistance.Database.models;
using KeepDistance.Server.models;
using RestSharp;
using Xamarin.Essentials;

namespace KeepDistance.Android.network
{
    public class NetworkInstructions
    {
        private readonly DatabaseMutations _mutations;
        private RestClient _client;
        private TelemetrySession _lastSession;

        public NetworkInstructions(DatabaseMutations mutations)
        {
            _mutations = mutations;
            _client = new RestClient("https://localhost:5001/api/telemetry");
        }

        public async Task<bool> CreateSession(TelemetrySession session)
        {
            
            var networkSession = new NetworkTelemetry
            {
                Info = _mutations.GetUserInfo(),
                Session = session
            };
            _lastSession = session;
            _lastSession.States.Clear();
            
            var request = new RestRequest("", DataFormat.Json).AddJsonBody(networkSession);

            var response = await _client.PostAsync<object>(request);
            
            return true;
        }
        
        public async Task<bool> UpdateSession(TelemetryState state)
        {
            _lastSession.States.Add(state);
            
            var request = new RestRequest("", DataFormat.Json).AddJsonBody(_lastSession);
            var response = await _client.PostAsync<object>(request);
            
            return true;
        }
    }
}