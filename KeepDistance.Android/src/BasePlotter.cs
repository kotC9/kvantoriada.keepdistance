﻿using KeepDistance.Communicate.enums;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;

namespace KeepDistance.Android
{
    public class BasePlotter
    {
        protected const double Step = 0.25;
        protected PlotModel Model => _plot.Model;
        
        private readonly PlotView _plot;
        private readonly string _title;
        private readonly int _minX;
        private readonly int _maxX;
        private readonly double _minY;
        private readonly double _maxY;

        protected readonly LineSeries BaseLine = new LineSeries
        {
            Color = OxyColor.FromRgb(255, 182, 175)
        };

        protected double CurrentX;
        private double _oldminX;
        private double _oldmaxX;

        //время по оси X в минутах!
        protected BasePlotter(PlotView plot, string title, int minX = 0, int maxX = 6, double minY = 0,
            double maxY = 60)
        {
            _plot = plot;
            _title = title;
            _minX = minX;
            _maxX = maxX;
            _minY = minY;
            _maxY = maxY;

            _oldmaxX = maxX;

            plot.Model = CreatePlot();
        }

        private PlotModel CreatePlot()
        {
            var plotModel = new PlotModel
            {
                Title = _title,
                TitleFontWeight = 1,
                TitleFontSize = 12,
                PlotAreaBorderColor = OxyColor.FromArgb(64, 0, 0, 0)
            };

            plotModel.Axes.Add(new LinearAxis
            {
                IsPanEnabled = true,
                IsZoomEnabled = true,
                Position = AxisPosition.Left,
                Maximum = _maxY,
                Minimum = _minY,
                TicklineColor = OxyColor.FromRgb(255, 104, 89),

            });
            
            plotModel.Axes.Add(new LinearAxis
            {
                IsPanEnabled = true,
                IsZoomEnabled = true,
                Position = AxisPosition.Bottom,
                TextColor = OxyColors.Black,
                Minimum = _minX,
                Maximum = _maxX,
                TicklineColor = OxyColor.FromRgb(255, 104, 89),
                MajorStep = 0.5,
                Title = "Время(мин)"
            });


            plotModel.Series.Add(BaseLine);
            
            return plotModel;
        }

        protected void DrawRealtime(DataPointSeries series, double dataX, double step = Step)
        {
            series.Points.Add(new DataPoint(CurrentX, dataX));
            if (CurrentX > _maxX)
            {
                _oldminX += step;
                _oldmaxX += step;
                Model.Axes[1].Minimum = _oldminX;
                Model.Axes[1].Maximum = _oldmaxX;

                //_series.Points.RemoveAt(0);
            }

            CurrentX += step;
        }

        public void Update()
        {
            Model.InvalidatePlot(true);
        }
    }
}