﻿using KeepDistance.Communicate.enums;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;

namespace KeepDistance.Android
{
    public class NodeStatePlotter : BasePlotter
    {
        public new PlotModel Model => base.Model;
        private readonly LineSeries _seriesNotKeep = new LineSeries
        {
            Color = OxyColors.Transparent, MarkerType = MarkerType.Circle, MarkerStrokeThickness = 1,
            MarkerStroke = OxyColor.FromRgb(255, 104, 89),
            MarkerFill = OxyColor.FromRgb(255, 182, 175)
        };

        private readonly LineSeries _seriesKeep = new LineSeries
        {
            Color = OxyColors.Transparent, MarkerType = MarkerType.Circle, MarkerStrokeThickness = 1,
            MarkerStroke = OxyColor.FromRgb(87, 115, 250),
            MarkerFill = OxyColor.FromRgb(169, 183, 252)
        };

        public NodeStatePlotter(PlotView plot, string title, double defaultX = 0, double minY = 0, double maxY = 150, int minX = 0,
            int maxX = 6) :
            base(plot, title, minX, maxX, minY, maxY)
        {
            Model.Axes[0].MajorStep = 50;
            Model.Axes[0].Title = "Дистанция(см)";
            Model.Series.Add(_seriesNotKeep);
            Model.Series.Add(_seriesKeep);

            CurrentX = defaultX;
        }

        public void DrawDistance(int distance)
        {
            //не ну это гениально конечно, надо бы исправить потом
            if (distance >= 150)
            {
                DrawRealtime(BaseLine, 75);
                CurrentX -= Step;
                DrawRealtime(_seriesKeep, 75);
            }
            else
            {
                DrawRealtime(BaseLine, distance);
                CurrentX -= Step;
                DrawRealtime(_seriesNotKeep, distance);
            }
            Update();
        }
    }
}