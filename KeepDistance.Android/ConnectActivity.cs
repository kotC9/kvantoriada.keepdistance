﻿using System;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using KeepDistance.Android.location;
using KeepDistance.Android.models;

namespace KeepDistance.Android
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppThemeNoActionBar")]
    public class ConnectActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_connect);

            Connect();
        }

        private async void Connect()
        {
            if (await Task.Run(() => Global.Provider.Connect()))
            {
                RunOnUiThread(() =>
                {
                    FindViewById<TextView>(id: Resource.Id.textViewConnect).Text
                        = GetString(resId: Resource.String.bt_connected);
            
                    StartActivity(type: typeof(ControlActivity));
                    Finish();
                });
            }
            else
            {
                RunOnUiThread(() =>
                {
                    Toast.MakeText(this, GetString(resId: Resource.String.bt_cantconnect), ToastLength.Short)?.Show();
                    Finish();
                });
            }
        }
    }
}