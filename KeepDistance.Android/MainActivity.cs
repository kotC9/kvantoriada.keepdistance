﻿using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Util;
using Android.Widget;
using KeepDistance.Communicate.Providers;
using KeepDistance.Database;
using KeepDistance.Database.models;
using Xamarin.Essentials;
using Environment = System.Environment;

namespace KeepDistance.Android
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            Platform.Init(this, savedInstanceState);

            InitializeComponents();
            
            CreateNotificationChannel();
        }

        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channelName = "KeepDistance channel";
            var channelDescription = "This channel used by KeepDistance.Android";
            var channel = new NotificationChannel("KeepDistanceNotification", channelName, NotificationImportance.Default)
            {
                Description = channelDescription
            };

            var notificationManager = (NotificationManager) GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

        private void InitializeComponents()
        {
            Global.Provider = new BluetoothProvider();
            Global.DbService = new DatabaseMutations(
                new DatabaseProvider(Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                    "telemetryDb"));

            if (Global.DbService.GetUserInfo() == null)
            {
                Global.DbService.CreateUser(new UserInfo
                {
                    Name = "Ivan",
                    IsSickCoronavirus = false,
                    Token = "SafeDistanceNode_1"
                });
            }
            
            InitializeDropdown();

            var buttonConnect = FindViewById<FloatingActionButton>(Resource.Id.buttonConnect);
            buttonConnect.Click += ButtonConnectOnClick;

            var buttonRefresh = FindViewById<Button>(Resource.Id.buttonRefresh);
            buttonRefresh.Click += ButtonRefreshOnClick;
        }

        private void ButtonRefreshOnClick(object sender, EventArgs e)
        {
            InitializeDropdown();
        }

        private void ButtonConnectOnClick(object sender, EventArgs e)
        {
            if (Global.Provider.Device == null)
            {
                Log.Info("ButtonConnect", "device is null");
                return;
            }

            StartActivity(typeof(ConnectActivity));
        }

        private void InitializeDropdown()
        {
            var dropdown = FindViewById<Spinner>(Resource.Id.dropdownCommunicator);
            var communicatorDevices = Global.Provider.Devices;

            if (communicatorDevices == null) return;

            var names = communicatorDevices.Select(item => item.Name).ToArray();
            var adapter = new ArrayAdapter<string>(this, Resource.Layout.support_simple_spinner_dropdown_item, names);
            dropdown.Adapter = adapter;

            dropdown.ItemSelected += DropdownOnItemSelected;
        }

        private void DropdownOnItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var spinner = (Spinner) sender;

            var item = (string) spinner.GetItemAtPosition(e.Position);

            Global.Provider.Device = Global.Provider.Devices.Find(d => d.Name == item);
        }
    }
}