﻿using System.Net;
using System.Net.Http.Headers;
using KeepDistance.Database;
using KeepDistance.Database.models;
using KeepDistance.Server.models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KeepDistance.Backend.controllers
{
    [Route("api/[controller]")]
    public class TelemetryController : Controller
    {
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly DatabaseNetworkMutations _mutations;

        public TelemetryController(IWebHostEnvironment appEnvironment, DatabaseNetworkMutations mutations)
        {
            _appEnvironment = appEnvironment;
            _mutations = mutations;
        }
        
        // PUT Data
        [HttpPost]
        public HttpStatusCode Post([FromBody] NetworkTelemetry session)
        {
            return _mutations.Create(session) 
                ? HttpStatusCode.OK 
                : HttpStatusCode.InternalServerError;
        }
        
        //UPDATE Data
        [HttpPatch]
        public HttpStatusCode Patch([FromBody] NetworkTelemetry session)
        {
            return _mutations.Update(session) 
                ? HttpStatusCode.OK 
                : HttpStatusCode.InternalServerError;
        }
    }
}